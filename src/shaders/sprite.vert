#version 330 core
layout (location = 0) in vec4 vertex; // vec2 position, vec2 texture_coordinates

out vec2 texture_coordinates;

uniform mat4 model;
uniform mat4 projection;

void main() {
  texture_coordinates = vertex.zw;
  gl_Position = projection * model * vec4(vertex.xy, 0.0f, 1.0f);
}
