#pragma once

#include <glad/glad.h>
// GLFW comes after glad
#include "GameLevel.hpp"
#include "ResourceManager.hpp"
#include "SpriteRenderer.hpp"
#include "openal/SoundEngine.hpp"
#include <GLFW/glfw3.h>
#include <array>
#include <memory>

enum class GameState {
  Active,
  Menu,
  Win,
  Lose,
};

class GameObject;

class Game {
public:
  Game(int width, int height);
  void init();
  void process_input();
  void update(float delta);
  void render();
  void key_callback(GLFWwindow *window, int key, int scancode, int action, int mode);

private:
  static spdlog::logger *logger();

  GameState game_state_;
  std::array<bool, GLFW_KEY_LAST + 1> keys_{};

  ResourceManager resource_manager_;
  std::unique_ptr<SpriteRenderer> sprite_renderer_;
  std::unique_ptr<SoundEngine> sound_engine_;

  std::vector<GameLevel> levels_;
  std::size_t level_ = 0;
  std::unique_ptr<GameObject> player_;
  std::unique_ptr<GameObject> screen_;
  std::vector<GameObject> background_squares_;
  const glm::vec2 player_size_{50, 100};
  static constexpr float player_speed = 200.0f;

  const int width_;
  const int height_;
};
