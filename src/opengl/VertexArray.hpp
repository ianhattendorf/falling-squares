#pragma once

#include <glad/glad.h>
#include <memory>

#include "../spdlog_fwd.hpp"

class VertexArray {
public:
  VertexArray();
  explicit VertexArray(GLsizei n);
  VertexArray(const VertexArray &) = delete;
  VertexArray &operator=(const VertexArray &) = delete;
  VertexArray(VertexArray &&other) noexcept;
  VertexArray &operator=(VertexArray &&other) noexcept;
  ~VertexArray();

  void bind() const;
  static void unbind();

private:
  void release();

  static spdlog::logger *logger();

  GLuint vertex_array_id_;
};
