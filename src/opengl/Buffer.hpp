#pragma once

#include <glad/glad.h>
#include <memory>

#include "../spdlog_fwd.hpp"

class Buffer {
public:
  Buffer();
  Buffer(GLenum target, GLsizeiptr size, const void *data, GLenum usage);
  Buffer(const Buffer &) = delete;
  Buffer &operator=(const Buffer &) = delete;
  Buffer(Buffer &&other) noexcept;
  Buffer &operator=(Buffer &&other) noexcept;
  ~Buffer();

  void bind() const;
  void unbind() const;

private:
  void release();

  static spdlog::logger *logger();

  GLuint buffer_id_;
  GLenum target_;
};
