#include "Texture.hpp"

#include "GlUtil.hpp"
#include <algorithm>
#include <cassert>
#include <cstring>
#include <spdlog/spdlog.h>
#include <stb_image.h>

Texture::Texture() : texture_id_(0), type_(TextureType::Unknown) {
  logger()->debug("id[{}] constructor", texture_id_);
}

// texture_id_ initialized via glGenTextures
// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
Texture::Texture(const int width, const int height, const GLenum format, GLubyte *const data,
                 const TextureType texture_type)
    : type_(texture_type) {
  glGenTextures(1, &texture_id_);
  logger()->debug("id[{}] type [{}] constructor", texture_id_, get_type_string());
  glBindTexture(GL_TEXTURE_2D, texture_id_);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  // TODO detect features once at startup
  bool aniso_supported = false;
  GLint n;
  glGetIntegerv(GL_NUM_EXTENSIONS, &n);
  for (GLint i = 0; i < n; ++i) {
    const auto *extension_str =
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        reinterpret_cast<const char *>(glGetStringi(GL_EXTENSIONS, static_cast<GLuint>(i)));
    if (strcmp("GL_EXT_texture_filter_anisotropic", extension_str) == 0) {
      aniso_supported = true;
      break;
    }
  }

  if (aniso_supported) {
    float aniso;
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aniso);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso);
    logger()->debug("id[{}] aniso: {}", texture_id_, aniso);
  } else {
    logger()->debug("id[{}] aniso not supported", texture_id_);
  }

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
}

Texture::Texture(Texture &&other) noexcept : texture_id_(other.texture_id_), type_(other.type_) {
  other.texture_id_ = 0;
  other.type_ = TextureType::Unknown;
}

Texture &Texture::operator=(Texture &&other) noexcept {
  if (this != &other) {
    release();
    std::swap(texture_id_, other.texture_id_);
    std::swap(type_, other.type_);
  }
  return *this;
}

Texture::~Texture() {
  release();
}

void Texture::bind() const {
  assert(texture_id_ != 0);
  glBindTexture(GL_TEXTURE_2D, texture_id_);
}

void Texture::bind(const GLuint active_texture_number) const {
  Texture::set_active(active_texture_number);
  bind();
}

void Texture::set_active(const GLuint number) {
  // assert GL_MAX_TEXTURE_IMAGE_UNITS ?
  glActiveTexture(GL_TEXTURE0 + number);
}

void Texture::release() {
  if (texture_id_ != 0) {
    logger()->debug("id[{}].release()", texture_id_);
    glDeleteTextures(1, &texture_id_);
  }
}

TextureType Texture::get_type() const {
  return type_;
}

std::string Texture::get_type_string() const {
  return Texture::get_type_string(type_);
}

std::string Texture::get_type_string(TextureType type) {
  switch (type) {
  case TextureType::Diffuse:
    return "texture_diffuse";
  case TextureType::Specular:
    return "texture_specular";
  default:
    throw std::runtime_error("Unknown TextureType: " + std::to_string(static_cast<int>(type)));
  }
}

spdlog::logger *Texture::logger() {
  static auto instance = spdlog::stdout_color_mt("Texture");
  return instance.get();
}
