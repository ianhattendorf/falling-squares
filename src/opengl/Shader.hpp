#pragma once

#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <memory>
#include <string>

#include "../spdlog_fwd.hpp"

// Should probably break up individual shaders into separate classes
class Shader {
public:
  Shader();
  Shader(const std::string &vertex_source, const std::string &fragment_source);
  Shader(const Shader &) = delete;
  Shader &operator=(const Shader &) = delete;
  Shader(Shader &&other) noexcept;
  Shader &operator=(Shader &&other) noexcept;
  ~Shader();

  void use() const;
  GLint get_uniform_location(const std::string &name) const;
  void set_uniform(const std::string &name, int value) const;
  void set_uniform(const std::string &name, float value) const;
  void set_uniform(const std::string &name, const glm::mat4 &value) const;
  void set_uniform(const std::string &name, const glm::mat3 &value) const;
  void set_uniform(const std::string &name, const glm::vec3 &value) const;

private:
  GLuint compile_shader(const std::string &shader_source, GLenum type) const;
  void link_program(GLuint program) const;
  void release();

  static spdlog::logger *logger();

  GLuint program_id_;
  GLuint vertex_shader_id_;
  GLuint fragment_shader_id_;
};
