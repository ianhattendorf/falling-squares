#include "Buffer.hpp"

#include <algorithm>
#include <spdlog/spdlog.h>

Buffer::Buffer() : buffer_id_(0), target_(0) {
  logger()->debug("id[{}] target[{}] constructor", buffer_id_, target_);
}

// buffer_id_ initialized via glGenBuffers
// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
Buffer::Buffer(const GLenum target, const GLsizeiptr size, const void *const data,
               const GLenum usage)
    : target_(target) {
  glGenBuffers(1, &buffer_id_);
  logger()->debug("id[{}] target[{}] constructor", buffer_id_, target_);
  bind();
  glBufferData(target, size, data, usage);
}

Buffer::Buffer(Buffer &&other) noexcept : buffer_id_(other.buffer_id_), target_(other.target_) {
  other.buffer_id_ = 0;
  other.target_ = 0;
}

Buffer &Buffer::operator=(Buffer &&other) noexcept {
  if (this != &other) {
    release();
    std::swap(buffer_id_, other.buffer_id_);
    std::swap(target_, other.target_);
  }
  return *this;
}

Buffer::~Buffer() {
  release();
}

void Buffer::release() {
  if (buffer_id_ != 0) {
    glDeleteBuffers(1, &buffer_id_);
    logger()->debug("id[{}].release()", buffer_id_);
  }
}

void Buffer::bind() const {
  glBindBuffer(target_, buffer_id_);
}

spdlog::logger *Buffer::logger() {
  static auto instance = spdlog::stdout_color_mt("Buffer");
  return instance.get();
}

void Buffer::unbind() const {
  glBindBuffer(target_, 0);
}
