#pragma once

#include "../spdlog_fwd.hpp"
#include <glad/glad.h>
#include <memory>
#include <string>

enum class TextureType {
  Diffuse,
  Specular,
  Unknown,
};

class Texture {
public:
  Texture();
  Texture(int width, int height, GLenum format, GLubyte *data, TextureType texture_type);
  Texture(const Texture &) = delete;
  Texture &operator=(const Texture &) = delete;
  Texture(Texture &&other) noexcept;
  Texture &operator=(Texture &&other) noexcept;
  ~Texture();

  void bind() const;
  void bind(GLuint active_texture_number) const;
  static void set_active(GLuint number);

  TextureType get_type() const;
  std::string get_type_string() const;
  static std::string get_type_string(TextureType type);

private:
  void release();

  static spdlog::logger *logger();

  GLuint texture_id_;
  TextureType type_;
};
