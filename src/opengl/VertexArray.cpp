#include "VertexArray.hpp"

#include <algorithm>
#include <cassert>
#include <gsl/gsl_assert>
#include <spdlog/spdlog.h>

VertexArray::VertexArray() : vertex_array_id_(0) {
  logger()->debug("id({}) constructor", vertex_array_id_);
}

// vertex_array_id_ initialized via glGenVertexArrays
// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
VertexArray::VertexArray(const GLsizei n) {
  Expects(n == 1); // Multiple arrays not yet supported
  glGenVertexArrays(1, &vertex_array_id_);
  logger()->debug("id({}) constructor", vertex_array_id_);
  glBindVertexArray(vertex_array_id_);
}

VertexArray::VertexArray(VertexArray &&other) noexcept : vertex_array_id_(other.vertex_array_id_) {
  other.vertex_array_id_ = 0;
}

VertexArray &VertexArray::operator=(VertexArray &&other) noexcept {
  if (this != &other) {
    release();
    std::swap(vertex_array_id_, other.vertex_array_id_);
  }
  return *this;
}

VertexArray::~VertexArray() {
  release();
}

void VertexArray::bind() const {
  glBindVertexArray(vertex_array_id_);
}

void VertexArray::release() {
  if (vertex_array_id_ != 0) {
    logger()->debug("id({}).release()", vertex_array_id_);
    glDeleteVertexArrays(1, &vertex_array_id_);
  }
}

void VertexArray::unbind() {
  glBindVertexArray(0);
}

spdlog::logger *VertexArray::logger() {
  static auto instance = spdlog::stdout_color_mt("VertexArray");
  return instance.get();
}
