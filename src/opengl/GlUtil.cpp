#include "GlUtil.hpp"

#include <spdlog/spdlog.h>
#include <sstream>
#include <string>

void GlUtil::check_object_success(
    const GLuint object, const GLenum status,
    const std::function<void(GLuint, GLenum, GLint *)> &iv_func,
    const std::function<void(GLuint, GLsizei, GLsizei *, GLchar *)> &info_log_func) {
  GLint success;
  iv_func(object, status, &success);
  if (success == 0) {
    std::string info_log;
    constexpr GLsizei info_log_size = 1024;
    info_log.resize(info_log_size);

    info_log_func(object, info_log_size, nullptr, &info_log[0]);

    // Trim excess chars
    const std::size_t found = info_log.find('\0');
    if (found != std::string::npos) {
      info_log.resize(found + 1);
    }

    logger()->error(info_log);

    throw std::runtime_error("Vertex shader compilation failed.");
  }
}

spdlog::logger *GlUtil::logger() {
  static auto instance = spdlog::stdout_color_mt("GlUtil");
  return instance.get();
}

void GlUtil::gl_throw_if_error_(const char *const file, const int line) {
  GLenum error_code = glGetError();
  if (error_code == GL_NO_ERROR) {
    return;
  }

  std::stringstream error_message;
  std::string separator;
  error_message << "GL ERROR (" << file << ":" << line << "): ";
  while (error_code != GL_NO_ERROR) {
    error_message << separator;
    switch (error_code) {
    case GL_INVALID_ENUM:
      error_message << "GL_INVALID_ENUM";
      break;
    case GL_INVALID_VALUE:
      error_message << "GL_INVALID_VALUE";
      break;
    case GL_INVALID_OPERATION:
      error_message << "GL_INVALID_OPERATION";
      break;
    case GL_OUT_OF_MEMORY:
      error_message << "GL_OUT_OF_MEMORY";
      break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:
      error_message << "GL_INVALID_FRAMEBUFFER_OPERATION";
      break;
    default:
      error_message << "UNKNOWN";
      break;
    }
    separator = "|";
    error_code = glGetError();
  }
  throw std::runtime_error(error_message.str());
}
