#pragma once

#include "../spdlog_fwd.hpp"
#include <functional>
#include <glad/glad.h>
#include <memory>

#ifdef NDEBUG
#define gl_throw_if_error()
#else
#define gl_throw_if_error() GlUtil::gl_throw_if_error_(__FILE__, __LINE__)
#endif

class GlUtil {
public:
  static spdlog::logger *logger();
  static void check_object_success(
      GLuint object, GLenum status, const std::function<void(GLuint, GLenum, GLint *)> &iv_func,
      const std::function<void(GLuint, GLsizei, GLsizei *, GLchar *)> &info_log_func);
  static void gl_throw_if_error_(const char *file, int line);
};
