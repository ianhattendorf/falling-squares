#include "Shader.hpp"
#include "../Util.hpp"
#include "GlUtil.hpp"
#include <algorithm>
#include <cassert>
#include <glm/gtc/type_ptr.hpp>
#include <spdlog/spdlog.h>

Shader::Shader() : program_id_(0), vertex_shader_id_(0), fragment_shader_id_(0) {
  logger()->debug("id({}) constructor", program_id_);
}

Shader::Shader(const std::string &vertex_source, const std::string &fragment_source) {
  vertex_shader_id_ = compile_shader(vertex_source, GL_VERTEX_SHADER);
  fragment_shader_id_ = compile_shader(fragment_source, GL_FRAGMENT_SHADER);
  program_id_ = glCreateProgram();
  logger()->debug("id({}) constructor", program_id_);

  glAttachShader(program_id_, vertex_shader_id_);
  glAttachShader(program_id_, fragment_shader_id_);
  link_program(program_id_);

  glDeleteShader(vertex_shader_id_);
  glDeleteShader(fragment_shader_id_);
}

Shader::Shader(Shader &&other) noexcept
    : program_id_(other.program_id_), vertex_shader_id_(other.vertex_shader_id_),
      fragment_shader_id_(other.fragment_shader_id_) {
  other.program_id_ = 0;
  other.vertex_shader_id_ = 0;
  other.fragment_shader_id_ = 0;
}

Shader &Shader::operator=(Shader &&other) noexcept {
  if (this != &other) {
    release();
    std::swap(program_id_, other.program_id_);
    std::swap(vertex_shader_id_, other.vertex_shader_id_);
    std::swap(fragment_shader_id_, other.fragment_shader_id_);
  }
  return *this;
}

Shader::~Shader() {
  release();
}

void Shader::use() const {
  assert(program_id_ != 0);
  glUseProgram(program_id_);
}

GLint Shader::get_uniform_location(const std::string &name) const {
  return glGetUniformLocation(program_id_, name.c_str());
}

void Shader::set_uniform(const std::string &name, const int value) const {
  glUniform1i(get_uniform_location(name), value);
}

void Shader::set_uniform(const std::string &name, const float value) const {
  glUniform1f(get_uniform_location(name), value);
}

void Shader::set_uniform(const std::string &name, const glm::mat4 &value) const {
  // For now, set single uniform without transpose
  glUniformMatrix4fv(get_uniform_location(name), 1, GL_FALSE, glm::value_ptr(value));
}

void Shader::set_uniform(const std::string &name, const glm::mat3 &value) const {
  // For now, set single uniform without transpose
  glUniformMatrix3fv(get_uniform_location(name), 1, GL_FALSE, glm::value_ptr(value));
}

void Shader::set_uniform(const std::string &name, const glm::vec3 &value) const {
  glUniform3f(get_uniform_location(name), value.x, value.y, value.z);
}

GLuint Shader::compile_shader(const std::string &shader_source, const GLenum type) const {
  const GLuint shader = glCreateShader(type);
  const GLchar *gl_shader_source = shader_source.c_str();
  glShaderSource(shader, 1, &gl_shader_source, nullptr);
  glCompileShader(shader);

  try {
    GlUtil::check_object_success(shader, GL_COMPILE_STATUS, glGetShaderiv, glGetShaderInfoLog);
  } catch (const std::exception & /*e*/) {
    glDeleteShader(shader);
    throw;
  }

  return shader;
}

void Shader::link_program(GLuint program) const {
  glLinkProgram(program);
  GlUtil::check_object_success(program, GL_LINK_STATUS, glGetProgramiv, glGetProgramInfoLog);
}

void Shader::release() {
  if (program_id_ == 0) {
    return;
  }
  logger()->debug("id({}).release()", program_id_);
  if (vertex_shader_id_ != 0) {
    glDetachShader(program_id_, vertex_shader_id_);
  }
  if (fragment_shader_id_ != 0) {
    glDetachShader(program_id_, fragment_shader_id_);
  }
}

spdlog::logger *Shader::logger() {
  static auto instance = spdlog::stdout_color_mt("Shader");
  return instance.get();
}
