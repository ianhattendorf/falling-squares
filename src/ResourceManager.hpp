#pragma once

#include <memory>
#include <string>
#include <unordered_map>

#include "opengl/Shader.hpp"
#include "opengl/Texture.hpp"

// Should ResourceManager own all pointers and just return raw pointers?
class ResourceManager {
public:
  std::shared_ptr<Shader> load_shader(std::string name, const std::string &vertex_path,
                                      const std::string &fragment_path);
  std::shared_ptr<Shader> get_shader(const std::string &name) const;

  std::shared_ptr<Texture> load_texture(std::string name, const std::string &path);
  std::shared_ptr<Texture> get_texture(const std::string &name) const;

private:
  static spdlog::logger *logger();

  std::unordered_map<std::string, std::shared_ptr<Shader>> shaders_;
  std::unordered_map<std::string, std::shared_ptr<Texture>> textures_;

  static constexpr auto root_asset_texture_path = "src/assets/textures";
  static constexpr auto root_asset_shader_path = "src/shaders";
};
