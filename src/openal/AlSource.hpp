#pragma once

#include "../spdlog_fwd.hpp"
#include <AL/al.h>

class AlBuffer;

class AlSource {
public:
  explicit AlSource(ALsizei number_of_sources);
  AlSource(const AlSource &) = delete;
  AlSource &operator=(const AlSource &) = delete;
  AlSource(AlSource &&other) noexcept;
  AlSource &operator=(AlSource &&other) noexcept;
  ~AlSource();

  void set_buffer(ALuint buffer_id) const;
  void queue_buffers(AlBuffer *al_buffer) const;
  void queue_buffer(ALuint buffer_id) const;
  ALuint dequeue_buffer() const;
  void play() const;
  void pause() const;
  void stop() const;
  void set_gain(float gain) const;
  void set_looping(bool looping) const;

  bool is_playing() const;
  ALuint get_source_id() const;
  int get_num_buffers_processed() const;
  int get_num_buffers_queued() const;

private:
  void release();
  int get_param(ALenum param) const;

  static spdlog::logger *logger();

  ALuint source_id_;
  ALsizei number_of_sources_;
};
