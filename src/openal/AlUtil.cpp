#include "AlUtil.hpp"

#include <al.h>
#include <alc.h>
#include <cassert>
#include <exception>
#include <iterator>
#include <sstream>

void AlUtil::al_throw_if_error_(const char *file, int line) {
  ALenum error_code = alGetError();
  if (error_code == AL_NO_ERROR) {
    return;
  }

  std::stringstream error_message;
  std::string separator;
  error_message << "AL ERROR (" << file << ":" << line << "): ";
  for (std::ptrdiff_t count = 0; error_code != AL_NO_ERROR && count < 5; ++count) {
    error_message << separator;
    switch (error_code) {
    case AL_INVALID_ENUM:
      error_message << "AL_INVALID_ENUM";
      break;
    case AL_INVALID_VALUE:
      error_message << "AL_INVALID_VALUE";
      break;
    case AL_INVALID_OPERATION:
      error_message << "AL_INVALID_OPERATION";
      break;
    case AL_OUT_OF_MEMORY:
      error_message << "AL_OUT_OF_MEMORY";
      break;
    default:
      error_message << "UNKNOWN";
      break;
    }
    separator = "|";
    error_code = alGetError();
  }
  throw std::runtime_error(error_message.str());
}

std::vector<std::string> AlUtil::get_extensions(ALCdevice_struct *const device) {
  std::istringstream extensions_stream{device == nullptr ? alGetString(AL_EXTENSIONS)
                                                         : alcGetString(device, ALC_EXTENSIONS)};
  return {std::istream_iterator<std::string>(extensions_stream),
          std::istream_iterator<std::string>()};
}
