#include "OggStream.hpp"
#include "AlBuffer.hpp"
#include <gsl/gsl_assert>
#include <gsl/gsl_util>
#include <gsl/pointers>
#include <spdlog/spdlog.h>
#include <vorbis/vorbisfile.h>

void OggClear::operator()(OggVorbis_File *const ptr) const {
  if (ptr == nullptr) {
    return;
  }

  ov_clear(ptr);
  // Smart pointer custom deleter
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  delete ptr;
}

OggStream::OggStream(const std::string &path) {
  init_vorbis_file(path);

  {
    const vorbis_info *const vi = ov_info(vorbis_file_.get(), -1);
    char **ptr = ov_comment(vorbis_file_.get(), -1)->user_comments;
    while (*ptr != nullptr) {
      // C library interface
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      logger()->debug("{}", *ptr++);
    }
    logger()->debug("Bitstream is {} channel(s), {}Hz", vi->channels, vi->rate);
    logger()->debug("Decoded length: {} samples", ov_pcm_total(vorbis_file_.get(), -1));
    logger()->debug("Encoded by: {}", ov_comment(vorbis_file_.get(), -1)->vendor);
    Expects(vi->channels > 0 && vi->rate > 0);
    channels_ = vi->channels;
    rate_ = vi->rate;
  }
}

std::ptrdiff_t OggStream::fill_buffer(std::vector<char> &buffer, const bool read_all) {
  constexpr int read_request_size = 4 * 1024;
  Expects(!reached_end_of_file());
  Expects(read_request_size <= buffer.size());

  std::ptrdiff_t read_so_far = 0;
  while (read_so_far + read_request_size <= gsl::narrow_cast<std::ptrdiff_t>(buffer.size())) {
    const long read_size =
        ov_read(vorbis_file_.get(), &buffer[static_cast<std::size_t>(read_so_far)],
                read_request_size, 0, word_size_, 1, &bitstream_);
    if (read_size < 0) {
      throw std::runtime_error("Error while reading Ogg file");
    }
    if (read_size == 0) {
      end_of_file_ = true;
      break;
    }
    read_so_far += read_size;
    // Resize buffer as needed if we want to read everything
    // Maybe want to configure resize amount?
    // Could also use ov_pcm_total to calculate exact buffer size
    if (read_all &&
        read_so_far + read_request_size > gsl::narrow_cast<std::ptrdiff_t>(buffer.size())) {
      buffer.resize(static_cast<std::size_t>(read_so_far + read_request_size * 16));
    }
  }
  return read_so_far;
}

bool OggStream::rewind() {
  const bool rewind_success = ov_pcm_seek(vorbis_file_.get(), 0) == 0;
  if (rewind_success) {
    end_of_file_ = false;
  }
  return rewind_success;
}

bool OggStream::reached_end_of_file() const {
  return end_of_file_;
}

int OggStream::get_channels() const {
  return channels_;
}

short OggStream::get_bits_per_sample() const {
  return static_cast<short>(word_size_ * 8);
}

long OggStream::get_rate() const {
  return rate_;
}

void OggStream::init_vorbis_file(const std::string &path) {
  Expects(!file_.is_open());

  // NOLINTNEXTLINE(hicpp-signed-bitwise)
  file_.open(path, std::ios_base::in | std::ios_base::binary);
  if (!file_) {
    throw std::runtime_error("Failed to open file: " + path);
  }

  // OggVorbis_File needs to be initialized via ov_open_callbacks,
  // which needs a raw pointer (memset is called internally)
  // NOLINTNEXTLINE(hicpp-use-auto, modernize-use-auto)
  const gsl::owner<OggVorbis_File *> vf = new OggVorbis_File;
  const ov_callbacks ov_ifstream_callback{istream_read, istream_seek, istream_close, istream_tell};
  if (ov_open_callbacks(&file_, vf, nullptr, 0, ov_ifstream_callback) < 0) {
    delete vf;
    throw std::runtime_error("File not found/unreadable/not Ogg Vorbis: " + path);
  }
  vorbis_file_.reset(vf);
}

spdlog::logger *OggStream::logger() {
  static auto instance = spdlog::stdout_color_mt("OggStream");
  return instance.get();
}

size_t OggStream::istream_read(void *const ptr, const size_t size, const size_t nmemb,
                               void *const datasource) {
  auto *stream = static_cast<std::istream *>(datasource);

  stream->read(static_cast<char *>(ptr), static_cast<std::streamsize>(size * nmemb));
  return static_cast<size_t>(stream->gcount()) / size;
}

int OggStream::istream_seek(void *const datasource, const ogg_int64_t offset, const int whence) {
  auto *stream = static_cast<std::istream *>(datasource);

  switch (whence) {
  case SEEK_CUR:
    stream->seekg(offset, std::ios_base::cur);
    break;
  case SEEK_SET:
    stream->seekg(offset, std::ios_base::beg);
    break;
  case SEEK_END:
    stream->seekg(offset, std::ios_base::end);
    break;
  default:
    Expects(false); // See comment below re: libvorbis
    return -1;
  }

  const auto tellg = gsl::narrow<int>(stream->tellg());
  // libvorbis tries to free uninitialized stack variable in some cases if seek returns < 0
  // https://github.com/xiph/vorbis/issues/42
  assert(tellg >= 0);
  return tellg;
}

int OggStream::istream_close(void * /*datasource*/) {
  return 0;
}

long OggStream::istream_tell(void *const datasource) {
  auto *stream = static_cast<std::istream *>(datasource);

  return gsl::narrow<long>(stream->tellg());
}
