#include "SoundEngine.hpp"
#include "AlBuffer.hpp"
#include "AlSource.hpp"
#include "AlUtil.hpp"
#include "OggStream.hpp"
#include "Sound.hpp"
#include <AL/al.h>
#include <AL/alc.h>
#include <experimental/filesystem>
#include <fmt/ostream.h>
#include <fstream>
#include <gsl/gsl_assert>
#include <iterator>
#include <limits>
#include <spdlog/spdlog.h>
#include <sstream>
#include <stdexcept>
#include <string>

namespace fs = std::experimental::filesystem;

SoundEngine::SoundEngine() {
  openal_init();

  update_thread_ = std::thread([=]() {
    try {
      update_task();
    } catch (const std::exception &e) {
      logger()->critical("Audio thread crashed!");
      logger()->critical(e.what());
    }
  });
  logger()->info("Audio thread ID: {}", update_thread_.get_id());
}

SoundEngine::~SoundEngine() {
  audio_thread_enabled_ = false;
  update_thread_.join();
  sounds_.clear();
  al_throw_if_error();
}

// Should we default to streaming/not streaming? Stream music, not effects?
SoundHandle SoundEngine::play(std::string path, const bool stream) {
  // locks message_queue_

  const auto handle = next_sound_handle_++;

  SoundMessage message;
  message.type = SoundMessageType::PlayCreate;
  message.handle = handle;
  message.path = std::move(path);
  message.stream = stream;

  const std::lock_guard<std::mutex> lock{mutex_message_queue_};
  message_queue_.push(std::move(message));

  return handle;
}

void SoundEngine::play(const SoundHandle handle) {
  // locks message_queue_

  // TODO valid handle?
  SoundMessage message;
  message.type = SoundMessageType::Play;
  message.handle = handle;

  const std::lock_guard<std::mutex> lock{mutex_message_queue_};
  message_queue_.push(std::move(message));
}

void SoundEngine::pause(const SoundHandle handle) {
  // locks message_queue_

  // TODO valid handle?
  SoundMessage message;
  message.type = SoundMessageType::Pause;
  message.handle = handle;

  const std::lock_guard<std::mutex> lock{mutex_message_queue_};
  message_queue_.push(std::move(message));
}

void SoundEngine::stop(const SoundHandle handle) {
  // locks message_queue_

  // TODO valid handle?
  SoundMessage message;
  message.type = SoundMessageType::Stop;
  message.handle = handle;

  const std::lock_guard<std::mutex> lock{mutex_message_queue_};
  message_queue_.push(std::move(message));
}

void SoundEngine::set_gain(const SoundHandle handle, const float gain) {
  // locks message_queue_

  // TODO valid handle?
  SoundMessage message;
  message.type = SoundMessageType::Gain;
  message.handle = handle;
  message.gain = gain;

  const std::lock_guard<std::mutex> lock{mutex_message_queue_};
  message_queue_.push(std::move(message));
}

void SoundEngine::set_looping(const SoundHandle handle, const bool looping) {
  // locks message_queue_

  // TODO valid handle?
  SoundMessage message;
  message.type = SoundMessageType::Looping;
  message.handle = handle;
  message.looping = looping;

  const std::lock_guard<std::mutex> lock{mutex_message_queue_};
  message_queue_.push(std::move(message));
}

void SoundEngine::update_task() {
  // First lock message_queue_, potentially lock sounds_

  while (audio_thread_enabled_) {
    {
      // Can we check if queue is not empty first to reduce unnecessary locking?
      const std::lock_guard<std::mutex> lock_message_queue{mutex_message_queue_};
      while (!message_queue_.empty()) {
        const SoundMessage message = message_queue_.front();
        message_queue_.pop();

        // Try not to spend too much time here as we're blocking any other threads trying to play
        // or modify any sounds. Right now most sound methods are fairly lightweight, so this
        // shouldn't be an issue currently.
        switch (message.type) {
        case SoundMessageType::PlayCreate: {
          const std::string sound_path =
              (fs::path(root_asset_sound_path) / fs::path(message.path)).string();
          auto sound = std::make_unique<Sound>(sound_path, message.stream);
          sound->play();

          const std::lock_guard<std::mutex> lock_sounds{mutex_sounds_};
          sounds_.emplace(message.handle, std::move(sound));

          break;
        }
        case SoundMessageType::Play: {
          sounds_.at(message.handle)->play();
          break;
        }
        case SoundMessageType::Pause: {
          sounds_.at(message.handle)->pause();
          break;
        }
        case SoundMessageType::Stop: {
          sounds_.at(message.handle)->stop();
          break;
        }
        case SoundMessageType::Gain: {
          sounds_.at(message.handle)->set_gain(message.gain);
          break;
        }
        case SoundMessageType::Looping: {
          sounds_.at(message.handle)->set_looping(message.looping);
          break;
        }
        default:
          throw std::runtime_error("Unknown message type");
        }
      }
    }

    // sounds_ is only read from other threads to check if handle exists, no need to lock here
    for (auto &sound : sounds_) {
      sound.second->update();
    }

    al_throw_if_error();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
}

bool SoundEngine::valid_handle(const SoundHandle handle) const {
  return sounds_.find(handle) != sounds_.cend();
}

void SoundEngine::openal_init() {
  Expects(device_ == nullptr);
  Expects(context_ == nullptr);

  device_.reset(alcOpenDevice(nullptr));
  if (device_ == nullptr) {
    throw std::runtime_error("Unable to open OpenAL device");
  }
  context_.reset(alcCreateContext(device_.get(), nullptr));
  alcMakeContextCurrent(context_.get());
  al_throw_if_error();

  // Print OpenAL device and extension info
  {
    const bool enumeration_supported =
        alcIsExtensionPresent(nullptr, "ALC_ENUMERATION_EXT") == AL_TRUE;
    const char *devices = alcGetString(nullptr, enumeration_supported ? ALC_ALL_DEVICES_SPECIFIER
                                                                      : ALC_DEVICE_SPECIFIER);
    logger()->info("OpenAL Devices:");
    if (devices == nullptr || *devices == '\0') {
      logger()->warn("No OpenAL devices detected");
    } else {
      do {
        logger()->info("\t{}", devices);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        devices += std::strlen(devices) + 1;
      } while (*devices != '\0');
    }
    logger()->info("Default Device: {}",
                   alcGetString(nullptr, enumeration_supported ? ALC_DEFAULT_ALL_DEVICES_SPECIFIER
                                                               : ALC_DEFAULT_DEVICE_SPECIFIER));

    std::istringstream al_extensions_stream{alGetString(AL_EXTENSIONS)};
    const std::vector<std::string> al_extensions = AlUtil::get_extensions();
    const std::vector<std::string> alc_extensions = AlUtil::get_extensions(device_.get());

    logger()->info("AL Extensions:");
    for (const auto &extension : al_extensions) {
      logger()->info("\t{}", extension);
    }
    logger()->info("ALC Extensions:");
    for (const auto &extension : alc_extensions) {
      logger()->info("\t{}", extension);
    }

    logger()->info("Using device: {}", alcGetString(device_.get(), ALC_DEVICE_SPECIFIER));
    const bool extension_hrtf_present = std::find(alc_extensions.cbegin(), alc_extensions.cend(),
                                                  "ALC_SOFT_HRTF") != alc_extensions.cend();
    const bool extension_latency_present =
        std::find(al_extensions.cbegin(), al_extensions.cend(), "AL_SOFT_source_latency") !=
        al_extensions.cend();
    logger()->info("HRTF supported: {}", extension_hrtf_present);
    logger()->info("Latency measurement supported: {}", extension_latency_present);
  }

  AL_CHECKED(alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f));

  al_throw_if_error();
}

spdlog::logger *SoundEngine::logger() {
  static auto instance = spdlog::stdout_color_mt("SoundEngine");
  return instance.get();
}

void AlCloseDevice::operator()(ALCdevice_struct *const ptr) const {
  if (ptr == nullptr) {
    return;
  }

  alcCloseDevice(ptr);
}

void AlCloseContext::operator()(ALCcontext_struct *const ptr) const {
  if (ptr == nullptr) {
    return;
  }

  // Not thread-safe
  const auto current_context = alcGetCurrentContext();
  if (current_context == ptr) {
    alcMakeContextCurrent(nullptr);
  }
  alcDestroyContext(ptr);
}
