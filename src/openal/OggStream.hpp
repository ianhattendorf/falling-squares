#pragma once

#include "../Application.hpp"
#include "../spdlog_fwd.hpp"
#include "AudioStream.hpp"
#include <al.h>
#include <fstream>
#include <string>

struct OggVorbis_File;

struct OggClear {
  void operator()(OggVorbis_File *ptr) const;
};

class OggStream : public AudioStream {
public:
  explicit OggStream(const std::string &path);

  std::ptrdiff_t fill_buffer(std::vector<char> &buffer, bool read_all) override;
  bool rewind() override;
  bool reached_end_of_file() const override;
  int get_channels() const override;
  short get_bits_per_sample() const override;
  long get_rate() const override;

private:
  void init_vorbis_file(const std::string &path);

  static spdlog::logger *logger();

  static size_t istream_read(void *ptr, size_t size, size_t nmemb, void *datasource);
  static int istream_seek(void *datasource, ogg_int64_t offset, int whence);
  static int istream_close(void *);
  static long istream_tell(void *datasource);

  std::ifstream file_;
  std::unique_ptr<OggVorbis_File, OggClear> vorbis_file_;
  int bitstream_ = 0;
  int channels_;
  long rate_;
  bool end_of_file_ = false;

  static constexpr int word_size_ = 2;
  static_assert(word_size_ == 1 || word_size_ == 2, "word_size_ must be 1 or 2");
};
