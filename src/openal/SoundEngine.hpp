#pragma once

#include "../spdlog_fwd.hpp"
#include <atomic>
#include <memory>
#include <mutex>
#include <ogg/ogg.h>
#include <queue>
#include <thread>
#include <unordered_map>

struct ALCdevice_struct;
struct ALCcontext_struct;
class Sound;

struct AlCloseDevice {
  void operator()(ALCdevice_struct *ptr) const;
};

struct AlCloseContext {
  void operator()(ALCcontext_struct *ptr) const;
};

using SoundHandle = unsigned int;

enum class SoundMessageType {
  PlayCreate,
  Play,
  Pause,
  Stop,
  Gain,
  Looping,
};

struct SoundMessage {
  SoundMessageType type;
  SoundHandle handle;
  std::string path;
  float gain;
  bool looping;
  bool stream;
};

// Not thread-safe
class SoundEngine {
public:
  SoundEngine();
  SoundEngine(const SoundEngine &) = delete;
  SoundEngine(SoundEngine &&) = delete;
  SoundEngine operator=(const SoundEngine &) = delete;
  SoundEngine operator=(SoundEngine &&) = delete;
  ~SoundEngine();

  SoundHandle play(std::string path, bool stream);
  void play(SoundHandle handle);
  void pause(SoundHandle handle);
  void stop(SoundHandle handle);
  void set_gain(SoundHandle handle, float gain);
  void set_looping(SoundHandle handle, bool looping);

private:
  void openal_init();
  void update_task();
  bool valid_handle(SoundHandle handle) const;

  static spdlog::logger *logger();

  std::unique_ptr<ALCdevice_struct, AlCloseDevice> device_;
  std::unique_ptr<ALCcontext_struct, AlCloseContext> context_;
  bool audio_thread_enabled_ = true;
  std::thread update_thread_;
  std::atomic<SoundHandle> next_sound_handle_ = 1u;
  std::unordered_map<SoundHandle, std::unique_ptr<Sound>> sounds_;
  std::mutex mutex_sounds_;
  std::queue<SoundMessage> message_queue_;
  std::mutex mutex_message_queue_;

  static constexpr auto root_asset_sound_path = "src/assets/sounds";
};
