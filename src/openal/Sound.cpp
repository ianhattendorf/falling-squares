#include "Sound.hpp"
#include "AlBuffer.hpp"
#include "AlSource.hpp"
#include "AlUtil.hpp"
#include "OggStream.hpp"
#include "SndStream.hpp"
#include <experimental/filesystem>
#include <gsl/gsl_assert>
#include <gsl/gsl_util>
#include <spdlog/spdlog.h>

namespace fs = std::experimental::filesystem;

Sound::Sound(std::string path, bool streaming)
    : streaming_(streaming), stream_path_(std::move(path)) {}

void Sound::play() {
  if (buffers_initialized_) {
    al_source_->play();
  }
  sound_state_ = SoundState::Playing;
}

void Sound::pause() {
  if (buffers_initialized_) {
    al_source_->pause();
  }
  sound_state_ = SoundState::Paused;
}

void Sound::stop() {
  if (buffers_initialized_) {
    al_source_->stop();
  }
  sound_state_ = SoundState::Stopped;
}

void Sound::set_gain(const float gain) const {
  if (buffers_initialized_) {
    al_source_->set_gain(gain);
  }
}

void Sound::set_looping(const bool looping) {
  looping_ = looping;
  if (!streaming_) {
    al_source_->set_looping(looping);
  }
}

// TODO handle playback finishing
void Sound::update() {
  init();

  if (!streaming_ || sound_state_ != SoundState::Playing ||
      (!looping_ && stream_->reached_end_of_file())) {
    return;
  }

  const int number_of_buffers_processed = al_source_->get_num_buffers_processed();
  if (number_of_buffers_processed > 1) {
    logger()->debug("Processed {} buffers", number_of_buffers_processed);
  }

  // Fill all buffers
  // Should we limit number of buffers we fill
  // per update (i.e. fill std::min(2, number_of_buffers_processed) buffers)?
  for (int i = 0; i < number_of_buffers_processed; ++i) {
    auto bytes_read = stream_->fill_buffer(stream_buffer_, !streaming_);
    if (bytes_read == 0) {
      if (!looping_) {
        return;
      }
      if (!stream_->rewind()) {
        throw std::runtime_error("Failed to rewind stream");
      }
      bytes_read = stream_->fill_buffer(stream_buffer_, !streaming_);
      Expects(bytes_read != 0);
    }
    const ALuint buffer_id = al_source_->dequeue_buffer();
    al_buffer_->set_buffer_data(stream_buffer_, bytes_read, get_stream_format(),
                                stream_->get_rate(), al_buffer_->get_id_index(buffer_id));
    al_source_->queue_buffer(buffer_id);
  }

  // Source will stop playing if queue empties
  // If no buffers are queued, playback is finished
  if (!al_source_->is_playing() && al_source_->get_num_buffers_queued() != 0) {
    logger()->debug("Sound buffer underrun, restarting");
    al_source_->play();
  }

  al_throw_if_error();
}

SoundState Sound::get_sound_state() const {
  return sound_state_;
}

ALenum Sound::get_stream_format() const {
  if (stream_->get_channels() == 1) {
    if (stream_->get_bits_per_sample() == 8) {
      return AL_FORMAT_MONO8;
    }
    if (stream_->get_bits_per_sample() == 16) {
      return AL_FORMAT_MONO16;
    }
  }
  if (stream_->get_channels() == 2u) {
    if (stream_->get_bits_per_sample() == 8) {
      return AL_FORMAT_STEREO8;
    }
    if (stream_->get_bits_per_sample() == 16) {
      return AL_FORMAT_STEREO16;
    }
  }
  throw std::runtime_error("Unknown stream format");
}

void Sound::init() {
  if (buffers_initialized_) {
    return;
  }

  stream_ = Sound::get_stream(stream_path_);
  al_buffer_ = std::make_unique<AlBuffer>(streaming_ ? 4 : 1);
  al_source_ = std::make_unique<AlSource>(1);

  // Initialize buffers for static playback
  if (!streaming_) {
    const auto buffer_id = al_buffer_->get_ids()[0];
    const auto bytes_read = stream_->fill_buffer(stream_buffer_, !streaming_);
    al_buffer_->set_buffer_data(stream_buffer_, bytes_read, get_stream_format(),
                                stream_->get_rate(), 0);
    stream_buffer_.clear();
    stream_buffer_.shrink_to_fit();
    al_source_->set_buffer(buffer_id);
    if (sound_state_ == SoundState::Playing) {
      al_source_->play();
    }

    buffers_initialized_ = true;
    return;
  }

  // Initialize buffers for streaming playback
  stream_buffer_.resize(16 * 1024);
  const auto buffer_id_count = gsl::narrow_cast<std::ptrdiff_t>(al_buffer_->get_ids().size());
  for (std::ptrdiff_t i = 0; i < buffer_id_count; ++i) {
    const auto bytes_read = stream_->fill_buffer(stream_buffer_, !streaming_);
    if (bytes_read == 0) {
      // TODO support reading files smaller than total buffer size
      // Take into account looping
      throw std::runtime_error("Reached EOF before filling initial buffers");
    }
    al_buffer_->set_buffer_data(stream_buffer_, bytes_read, get_stream_format(),
                                stream_->get_rate(), i);
  }
  // Do we need to handle the case where we weren't able to fill all buffers (small files)?
  al_source_->queue_buffers(al_buffer_.get());
  if (sound_state_ == SoundState::Playing) {
    al_source_->play();
  }

  buffers_initialized_ = true;
}

spdlog::logger *Sound::logger() {
  static auto instance = spdlog::stdout_color_mt("Sound");
  return instance.get();
}

std::unique_ptr<AudioStream> Sound::get_stream(const std::string &path) {
  const std::string extension = fs::path(path).extension().string();
  if (extension == ".ogg") {
    return std::make_unique<OggStream>(path);
  }
  return std::make_unique<SndStream>(path);
}
