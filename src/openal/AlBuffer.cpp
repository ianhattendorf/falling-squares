#include "AlBuffer.hpp"
#include "AlUtil.hpp"

#include <algorithm>
#include <gsl/gsl_assert>
#include <gsl/gsl_util>
#include <spdlog/spdlog.h>
#include <sstream>
#include <string>

// al_buffer_ids_ initialized via alGenBuffers
// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
AlBuffer::AlBuffer(const int number_of_al_buffers) {
  Expects(number_of_al_buffers > 0);
  al_buffer_ids_.resize(static_cast<std::size_t>(number_of_al_buffers));
  AL_CHECKED(alGenBuffers(number_of_al_buffers, al_buffer_ids_.data()));
  logger()->debug("id[{}] constructor", ids_to_string());
}

AlBuffer::AlBuffer(AlBuffer &&other) noexcept : al_buffer_ids_(std::move(other.al_buffer_ids_)) {}

AlBuffer &AlBuffer::operator=(AlBuffer &&other) noexcept {
  if (this != &other) {
    release();
    std::swap(al_buffer_ids_, other.al_buffer_ids_);
  }
  return *this;
}

AlBuffer::~AlBuffer() {
  release();
}

void AlBuffer::set_buffer_data(const std::vector<char> &data, std::ptrdiff_t data_size,
                               ALenum format, long rate, std::ptrdiff_t buffer_index) const {
  AL_CHECKED(alBufferData(al_buffer_ids_[static_cast<std::size_t>(buffer_index)], format,
                          data.data(), gsl::narrow_cast<ALsizei>(data_size),
                          gsl::narrow_cast<ALsizei>(rate)));
}

const std::vector<ALuint> &AlBuffer::get_ids() const {
  return al_buffer_ids_;
}

std::ptrdiff_t AlBuffer::get_id_index(const ALuint id) const {
  const auto found = std::find(al_buffer_ids_.cbegin(), al_buffer_ids_.cend(), id);
  assert(found != al_buffer_ids_.cend());
  return std::distance(al_buffer_ids_.cbegin(), found);
}

void AlBuffer::release() {
  if (!al_buffer_ids_.empty()) {
    AL_CHECKED(alDeleteBuffers(static_cast<ALsizei>(al_buffer_ids_.size()), al_buffer_ids_.data()));
    logger()->debug("id[{}].release()", ids_to_string());
    al_buffer_ids_.clear();
  }
}

std::string AlBuffer::ids_to_string() const {
  std::stringstream ret;
  std::string separator;
  for (const ALuint id : al_buffer_ids_) {
    ret << separator << id;
    separator = ",";
  }
  return ret.str();
}

spdlog::logger *AlBuffer::logger() {
  static auto instance = spdlog::stdout_color_mt("AlBuffer");
  return instance.get();
}
