#pragma once

#include <vector>

class AudioStream {
public:
  AudioStream();
  AudioStream(const AudioStream &other);
  AudioStream(AudioStream &&other) noexcept;
  AudioStream &operator=(const AudioStream &other);
  AudioStream &operator=(AudioStream &&other) noexcept;
  virtual ~AudioStream();

  virtual std::ptrdiff_t fill_buffer(std::vector<char> &buffer, bool read_all) = 0;
  virtual bool rewind() = 0;
  virtual bool reached_end_of_file() const = 0;
  virtual int get_channels() const = 0;
  virtual short get_bits_per_sample() const = 0;
  virtual long get_rate() const = 0;
};
