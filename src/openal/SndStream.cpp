#include "SndStream.hpp"
#include <cassert>
#include <cstring>
#include <gsl/gsl_util>
#include <sndfile.hh>

SndStream::SndStream(const std::string &path)
    : sndfile_handle_(std::make_unique<SndfileHandle>(path)) {}

std::ptrdiff_t SndStream::fill_buffer(std::vector<char> &buffer, const bool read_all) {
  // Sample is a single audio sample, i.e. a single short value
  // A frame is n samples for n-channel audio

  const int num_channels = get_channels();
  if (read_all) {
    buffer.resize(static_cast<std::size_t>(sndfile_handle_->frames() * num_channels * sample_size));
  }
  // Need to align by frames (sample_size * num_channels)
  const std::ptrdiff_t num_samples_to_read =
      gsl::narrow_cast<std::ptrdiff_t>(buffer.size()) / sample_size -
      gsl::narrow_cast<std::ptrdiff_t>(buffer.size()) / sample_size % num_channels;

  // Does `reinterpret_cast<short*>(buffer.data())` violate strict aliasing?
  // Let's be safe and copy for now
  std::vector<short> sample_buffer(static_cast<std::size_t>(num_samples_to_read));
  const sf_count_t num_samples_read =
      sndfile_handle_->read(sample_buffer.data(), static_cast<sf_count_t>(sample_buffer.size()));
  if (num_samples_read < 0) {
    throw std::runtime_error("Error reading sndfile_handle_: " + std::to_string(num_samples_read));
  }
  if (num_samples_read == 0) {
    end_of_file_ = true;
  }
  const sf_count_t num_bytes_read = num_samples_read * sample_size;
  assert(static_cast<std::size_t>(num_bytes_read) <= buffer.size());
  std::memcpy(buffer.data(), sample_buffer.data(), static_cast<std::size_t>(num_bytes_read));
  return num_bytes_read;
}

bool SndStream::rewind() {
  const bool rewind_success = sndfile_handle_->seek(0, SEEK_SET) == 0;
  if (rewind_success) {
    end_of_file_ = false;
  }
  return rewind_success;
}

bool SndStream::reached_end_of_file() const {
  return end_of_file_;
}

int SndStream::get_channels() const {
  const int num_channels = sndfile_handle_->channels();
  Expects(num_channels > 0);
  return num_channels;
}

short SndStream::get_bits_per_sample() const {
  return sample_size * 8u;
}

long SndStream::get_rate() const {
  const int rate = sndfile_handle_->samplerate();
  Expects(rate > 0);
  return rate;
}

// Required for unique_ptr forward declaration
SndStream::~SndStream() = default;
