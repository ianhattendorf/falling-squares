#pragma once

#include <string>
#include <vector>

#ifdef NDEBUG
#define al_throw_if_error()
#else
#define al_throw_if_error() AlUtil::al_throw_if_error_(__FILE__, __LINE__)
#endif

#define AL_CHECKED(f)                                                                              \
  al_throw_if_error();                                                                             \
  f;                                                                                               \
  al_throw_if_error();

struct ALCdevice_struct;

class AlUtil {
public:
  static void al_throw_if_error_(const char *file, int line);
  static std::vector<std::string> get_extensions(ALCdevice_struct *device = nullptr);
};
