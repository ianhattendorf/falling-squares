#include "AlSource.hpp"
#include "AlBuffer.hpp"
#include "AlUtil.hpp"
#include <algorithm>
#include <gsl/gsl_assert>
#include <spdlog/spdlog.h>

// source_id_ initialized via glGenAlSources
// NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
AlSource::AlSource(const ALsizei number_of_sources) : number_of_sources_(number_of_sources) {
  Expects(number_of_sources == 1); // Multiple sources not yet implemented
  AL_CHECKED(alGenSources(number_of_sources, &source_id_));
  logger()->debug("id[{}] constructor", source_id_);
  AL_CHECKED(alSource3f(source_id_, AL_POSITION, 0.0f, 0.0f, 0.0f));
}

AlSource::AlSource(AlSource &&other) noexcept
    : source_id_(other.source_id_), number_of_sources_(other.number_of_sources_) {
  other.source_id_ = 0;
  other.number_of_sources_ = 0;
}

AlSource &AlSource::operator=(AlSource &&other) noexcept {
  if (this != &other) {
    release();
    std::swap(source_id_, other.source_id_);
    std::swap(number_of_sources_, other.number_of_sources_);
  }
  return *this;
}

AlSource::~AlSource() {
  release();
}

void AlSource::set_buffer(const ALuint buffer_id) const {
  AL_CHECKED(alSourcei(source_id_, AL_BUFFER, static_cast<ALint>(buffer_id)));
}

void AlSource::queue_buffers(AlBuffer *const al_buffer) const {
  AL_CHECKED(alSourceQueueBuffers(source_id_,
                                  static_cast<ALsizei>(al_buffer->al_buffer_ids_.size()),
                                  al_buffer->al_buffer_ids_.data()));
}

void AlSource::queue_buffer(const ALuint buffer_id) const {
  AL_CHECKED(alSourceQueueBuffers(source_id_, 1, &buffer_id));
}

ALuint AlSource::dequeue_buffer() const {
  ALuint buffer_id;
  AL_CHECKED(alSourceUnqueueBuffers(source_id_, 1, &buffer_id));
  return buffer_id;
}

void AlSource::play() const {
  AL_CHECKED(alSourcePlay(source_id_));
}

void AlSource::pause() const {
  AL_CHECKED(alSourcePause(source_id_));
}

void AlSource::stop() const {
  AL_CHECKED(alSourceStop(source_id_));
}

void AlSource::set_gain(const float gain) const {
  Expects(gain >= 0.0f && gain <= 1.0f); // Or just clamp?
  AL_CHECKED(alSourcef(source_id_, AL_GAIN, gain));
}

void AlSource::set_looping(const bool looping) const {
  AL_CHECKED(alSourcei(source_id_, AL_LOOPING, looping ? AL_TRUE : AL_FALSE));
}

bool AlSource::is_playing() const {
  ALenum state;
  AL_CHECKED(alGetSourcei(source_id_, AL_SOURCE_STATE, &state));
  return state == AL_PLAYING;
}

ALuint AlSource::get_source_id() const {
  return source_id_;
}

int AlSource::get_num_buffers_processed() const {
  return get_param(AL_BUFFERS_PROCESSED);
}

int AlSource::get_num_buffers_queued() const {
  return get_param(AL_BUFFERS_QUEUED);
}

void AlSource::release() {
  if (source_id_ != 0) {
    AL_CHECKED(alDeleteSources(number_of_sources_, &source_id_));
    logger()->debug("id[{}].release()", source_id_);
    source_id_ = 0;
  }
}

int AlSource::get_param(const ALenum param) const {
  ALint value;
  AL_CHECKED(alGetSourcei(source_id_, param, &value));
  return value;
}

spdlog::logger *AlSource::logger() {
  static auto instance = spdlog::stdout_color_mt("AlSource");
  return instance.get();
}
