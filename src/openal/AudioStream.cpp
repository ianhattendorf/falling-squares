#include "AudioStream.hpp"

AudioStream::AudioStream() = default;

AudioStream::AudioStream(const AudioStream &) = default;

AudioStream::AudioStream(AudioStream &&) noexcept = default;

AudioStream &AudioStream::operator=(const AudioStream &) = default;

AudioStream &AudioStream::operator=(AudioStream &&) noexcept = default;

AudioStream::~AudioStream() = default;
