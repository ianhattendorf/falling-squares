#pragma once

#include "../spdlog_fwd.hpp"
#include <AL/al.h>
#include <string>
#include <vector>

class AlBuffer {
public:
  explicit AlBuffer(int number_of_al_buffers);
  AlBuffer(const AlBuffer &) = delete;
  AlBuffer &operator=(const AlBuffer &) = delete;
  AlBuffer(AlBuffer &&other) noexcept;
  AlBuffer &operator=(AlBuffer &&other) noexcept;
  ~AlBuffer();

  void set_buffer_data(const std::vector<char> &data, std::ptrdiff_t data_size, ALenum format,
                       long rate, std::ptrdiff_t buffer_index) const;

  const std::vector<ALuint> &get_ids() const;
  std::ptrdiff_t get_id_index(ALuint id) const;

private:
  void release();
  std::string ids_to_string() const;

  static spdlog::logger *logger();

  std::vector<ALuint> al_buffer_ids_;

  friend class AlSource;
};
