#pragma once

#include "../spdlog_fwd.hpp"
#include <al.h>
#include <memory>
#include <string>
#include <vector>

class AudioStream;
class AlBuffer;
class AlSource;

enum class SoundState {
  Playing,
  Paused,
  Stopped,
};

class Sound {
public:
  Sound(std::string path, bool streaming);
  Sound(const Sound &other) = delete;
  Sound(Sound &&other) noexcept = delete;
  Sound &operator=(const Sound &other) = delete;
  Sound &operator=(Sound &&other) noexcept = delete;
  ~Sound() = default;

  void play();
  void pause();
  void stop();
  void set_gain(float gain) const;
  void set_looping(bool looping);
  void update();

  SoundState get_sound_state() const;

private:
  ALenum get_stream_format() const;
  void init();

  static spdlog::logger *logger();
  static std::unique_ptr<AudioStream> get_stream(const std::string &path);

  std::unique_ptr<AudioStream> stream_;
  std::unique_ptr<AlBuffer> al_buffer_;
  std::unique_ptr<AlSource> al_source_;

  SoundState sound_state_ = SoundState::Stopped;
  bool buffers_initialized_ = false;
  bool looping_ = false;
  const bool streaming_;
  std::vector<char> stream_buffer_;
  std::string stream_path_;
};
