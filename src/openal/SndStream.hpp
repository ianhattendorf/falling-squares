#pragma once

#include "AudioStream.hpp"
#include <memory>
#include <string>

class SndfileHandle;

class SndStream : public AudioStream {
public:
  explicit SndStream(const std::string &path);
  SndStream(const SndStream &other) = delete;
  SndStream(SndStream &&other) noexcept = default;
  SndStream &operator=(const SndStream &other) = delete;
  SndStream &operator=(SndStream &&other) noexcept = default;
  ~SndStream() override;

  std::ptrdiff_t fill_buffer(std::vector<char> &buffer, bool read_all) override;
  bool rewind() override;
  bool reached_end_of_file() const override;
  int get_channels() const override;
  short get_bits_per_sample() const override;
  long get_rate() const override;

private:
  std::unique_ptr<SndfileHandle> sndfile_handle_;
  bool end_of_file_ = false;

  static constexpr int sample_size = 2;
  static_assert(sizeof(short) == sample_size, "Expected short to be 2 bytes");
};
