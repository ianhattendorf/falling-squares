#pragma once

#include "GameObject.hpp"
#include <vector>

class ResourceManager;

class GameLevel {
public:
  GameLevel(int num_squares, int width, int height, const ResourceManager *resource_manager);

  void render(const SpriteRenderer *renderer) const;

  std::vector<GameObject> squares_;
};
