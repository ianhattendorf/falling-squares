#pragma once

#include <glm/detail/type_mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <memory>

class Texture;
class SpriteRenderer;

class GameObject {
public:
  GameObject(const glm::vec2 &position, const glm::vec2 &size, const glm::vec2 &velocity,
             const glm::vec3 &color, std::shared_ptr<Texture> texture);

  void render(const SpriteRenderer *renderer) const;
  glm::mat4 get_model() const;
  bool collides(const GameObject &other) const;

  glm::vec2 position_;
  glm::vec2 size_;
  glm::vec2 velocity_;
  glm::vec3 color_;
  float rotation_;
  bool visible_;
  std::shared_ptr<Texture> texture_;
};
