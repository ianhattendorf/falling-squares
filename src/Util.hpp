#pragma once

#include <string>

class Util {
public:
  static std::string read_file_as_string(const std::string &path);
};
