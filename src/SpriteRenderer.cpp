#include "SpriteRenderer.hpp"
#include "Vertex.hpp"
#include "opengl/Shader.hpp"
#include "opengl/Texture.hpp"
#include <vector>

SpriteRenderer::SpriteRenderer(std::shared_ptr<Shader> shader)
    : shader_(std::move(shader)), vao_(1) {
  const std::vector<Vertex> vertices = {
      {0.0f, 1.0f, 0.0f, 1.0f}, {1.0f, 0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 0.0f, 0.0f},
      {0.0f, 1.0f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f, 1.0f}, {1.0f, 0.0f, 1.0f, 0.0f},
  };

  vbo_ = Buffer{GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(vertices.size() * sizeof(Vertex)),
                vertices.data(), GL_STATIC_DRAW};

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
                        reinterpret_cast<void *>(offsetof(Vertex, position)));
  glEnableVertexAttribArray(0);

  vbo_.unbind();
  VertexArray::unbind();
}

void SpriteRenderer::draw_sprite(const Texture *texture, const glm::mat4 &model,
                                 const glm::vec3 &color) const {
  shader_->use();

  shader_->set_uniform("model", model);
  shader_->set_uniform("sprite_color", color);

  texture->bind(0);

  vao_.bind();
  glDrawArrays(GL_TRIANGLES, 0, 6);
  VertexArray::unbind();
}
