#include "Game.hpp"
#include "GameObject.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <gsl/gsl_assert>
#include <gsl/gsl_util>
#include <spdlog/spdlog.h>
#include <vector>

Game::Game(const int width, const int height)
    : game_state_(GameState::Active), width_(width), height_(height) {
  Expects(width > 0 && height > 0);
}

void Game::process_input() {
  if (game_state_ != GameState::Active) {
    return;
  }
  float player_x_velocity = 0.0f;
  if (keys_[GLFW_KEY_A] || keys_[GLFW_KEY_LEFT]) {
    player_x_velocity -= player_speed;
  }
  if (keys_[GLFW_KEY_D] || keys_[GLFW_KEY_RIGHT]) {
    player_x_velocity += player_speed;
  }
  player_->velocity_.x = player_x_velocity;
}

void Game::update(const float delta) {
  if (game_state_ != GameState::Active) {
    return;
  }
  auto &level_squares = levels_[level_].squares_;

  // Update square positions and check if they are currently visible
  for (auto &square : level_squares) {
    square.position_ += square.velocity_ * delta;
    square.visible_ = screen_->collides(square);
  }
  // Remove squares that have passed the ground
  // If we end up using visibility for something else, need to recheck collision with screen
  // Could possibly be done more efficiently by swapping elements to remove with the element
  // at the end and then calling pop_back() since element order doesn't currently matter
  level_squares.erase(std::remove_if(level_squares.begin(), level_squares.end(),
                                     [&](const auto &square) {
                                       return !square.visible_ && square.position_.y > 0;
                                     }),
                      level_squares.end());

  // Update player position, clamping to screen (x-axis only currently)
  player_->position_ = glm::clamp(player_->position_ + player_->velocity_ * delta,
                                  glm::vec2{0, player_->position_.y},
                                  glm::vec2{width_ - player_->size_.x, player_->position_.y});

  // Check for collision with player
  for (const auto &square : level_squares) {
    if (!square.visible_) {
      continue;
    }
    if (player_->collides(square)) {
      logger()->warn("Game over!");
      sound_engine_->play("8bit_bomb_explosion.wav", false);
      game_state_ = GameState::Lose;
      return;
    }
  }
}

void Game::render() {
  if (game_state_ != GameState::Active) {
    return;
  }
  for (const auto &square : background_squares_) {
    square.render(sprite_renderer_.get());
  }

  levels_[level_].render(sprite_renderer_.get());

  player_->render(sprite_renderer_.get());
}

void Game::key_callback(GLFWwindow *const /*window*/, const int key, const int /*scancode*/,
                        const int action, const int /*mode*/) {
  const auto key_index = static_cast<unsigned int>(key);
  Expects(key_index < keys_.size());

  if (action == GLFW_PRESS) {
    // Bounds are checked in Expects
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index)
    keys_[key_index] = true;
  } else if (action == GLFW_RELEASE) {
    // Bounds are checked in Expects
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index)
    keys_[key_index] = false;
  }
}

void Game::init() {
  sound_engine_ = std::make_unique<SoundEngine>();
  {
    const auto sound_handle = sound_engine_->play("S31-Let the Games Begin.ogg", true);
    sound_engine_->set_gain(sound_handle, 0.5f);
    sound_engine_->set_looping(sound_handle, true);
  }

  auto shader = resource_manager_.load_shader("sprite", "sprite.vert", "sprite.frag");
  const auto square_texture = resource_manager_.load_texture("square", "square.png");
  const auto projection =
      glm::ortho(0.0f, static_cast<float>(width_), static_cast<float>(height_), 0.0f, -1.0f, 1.0f);
  shader->use();
  shader->set_uniform("texture_sampler", 0);
  shader->set_uniform("projection", projection);
  sprite_renderer_ = std::make_unique<SpriteRenderer>(std::move(shader));

  levels_.emplace_back(50, width_, height_, &resource_manager_);

  const glm::vec2 starting_position{(width_ - player_size_.x) / 2, height_ - player_size_.y};
  player_ = std::make_unique<GameObject>(starting_position, player_size_, glm::vec2{},
                                         glm::vec3{1.0f}, square_texture);
  screen_ = std::make_unique<GameObject>(glm::vec2{0, 0}, glm::vec2{width_, height_}, glm::vec2{},
                                         glm::vec3{}, nullptr);
  background_squares_ = {
      {{400, 100}, {75, 50}, {0, 0}, {1.0f, 0.0f, 0.0f}, square_texture},
      {{100, 100}, {100, 150}, {0, 0}, {0.0f, 1.0f, 0.0f}, square_texture},
      {{200, 300}, {100, 100}, {0, 0}, {0.0f, 0.0f, 1.0f}, square_texture},
  };
}

spdlog::logger *Game::logger() {
  static auto instance = spdlog::stdout_color_mt("Game");
  return instance.get();
}
