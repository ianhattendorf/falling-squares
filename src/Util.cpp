#include "Util.hpp"

#include <fstream>
#include <sstream>

std::string Util::read_file_as_string(const std::string &path) {
  std::ifstream f;
  // NOLINTNEXTLINE(hicpp-signed-bitwise)
  f.exceptions(std::ifstream::badbit | std::ifstream::failbit);

  try {
    f.open(path);
    std::stringstream result_stream;
    result_stream << f.rdbuf();
    return result_stream.str();
  } catch (const std::ifstream::failure & /*e*/) {
    throw std::runtime_error("Failed to open or read file as string: " + path);
  }
}
