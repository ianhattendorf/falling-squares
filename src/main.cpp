#include "Application.hpp"
#include "Game.hpp"

#include <gsl/span>
#include <spdlog/spdlog.h>
#include <sstream>

int main(int argc, char *argv[]) {
  const auto args = gsl::make_span(argv, argc);

#ifdef NDEBUG
  spdlog::set_level(spdlog::level::info);
#else
  spdlog::set_level(spdlog::level::debug);
#endif
  const auto logger = spdlog::stdout_color_mt("main");
  spdlog::stdout_color_mt("Global");
  logger->info("Executable location: {}", args[0]);

  const int msaa_samples = [args]() {
    if (args.size() <= 1) {
      return 0;
    }
    std::istringstream ss{args[1]};
    int samples = 0;
    ss >> samples;
    return samples;
  }();

  try {
    Application app{msaa_samples};
    app.run();
  } catch (const std::exception &e) {
    logger->critical(e.what());
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
