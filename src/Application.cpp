#include "Application.hpp"
#include "opengl/GlUtil.hpp"

#include <glad/glad.h>
// GLFW comes after glad
#include <GLFW/glfw3.h>
#include <spdlog/spdlog.h>

#include <chrono>

using game_clock = std::chrono::steady_clock;

Application::Application(const int msaa_samples)
    : game_(INITIAL_WIDTH, INITIAL_HEIGHT), msaa_samples_(msaa_samples) {
  logger()->debug("Using {} MSAA samples", msaa_samples_);
  glfwInit();
  // Smart pointer with custom deleter to manage glfw context
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  glfw_context_.reset(new bool(true));
  // Use 0 instead of GLFW_FALSE to support older versions of GLFW (before ~v3.2)
  glfwWindowHint(GLFW_RESIZABLE, 0);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, 1);
#endif
  if (msaa_samples_ > 0) {
    glfwWindowHint(GLFW_SAMPLES, msaa_samples_);
  }

  window_.reset(
      glfwCreateWindow(INITIAL_WIDTH, INITIAL_HEIGHT, "Falling Squares", nullptr, nullptr));
  if (window_ == nullptr) {
    throw std::runtime_error("Failed to create GLFW window");
  }
  glfwMakeContextCurrent(window_.get());
  glfwSetWindowUserPointer(window_.get(), this);
  glfwSetKeyCallback(window_.get(), key_callback);

  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
  if (gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)) == 0) {
    throw std::runtime_error("Failed to initialize GLAD");
  }
  logger()->info("Application initialized");

  // Enable MSAA
  if (msaa_samples_ > 0) {
    glEnable(GL_MULTISAMPLE);
  }
  glEnable(GL_CULL_FACE);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  int width, height;
  glfwGetWindowSize(window_.get(), &width, &height);
  glViewport(0, 0, width, height);

  game_.init();
}

void Application::run() {
  auto last_time = game_clock::now();

  // FPS stats
  float frame_time_sum = 0.0f;
  int frame_time_count = 0;
  auto last_fps_time = last_time;

  // Move to fixed update/variable render later on
  while (glfwWindowShouldClose(window_.get()) == 0) {
    const auto current_time = game_clock::now();
    const float delta =
        std::chrono::duration_cast<std::chrono::duration<float>>(current_time - last_time).count();
    last_time = current_time;

    // Update and display FPS
    ++frame_time_count;
    frame_time_sum += delta;
    if (current_time - last_fps_time > std::chrono::seconds(1)) {
      logger()->info(" FPS: {}", frame_time_count / frame_time_sum);
      frame_time_sum = 0.0f;
      frame_time_count = 0;
      last_fps_time = current_time;
    }

    glfwPollEvents();
    game_.process_input();

    game_.update(delta);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    game_.render();
    gl_throw_if_error();
    glfwSwapBuffers(window_.get());
  }
}

Application::~Application() = default;

spdlog::logger *Application::logger() {
  static auto instance = spdlog::stdout_color_mt("Application");
  return instance.get();
}

void Application::key_callback(GLFWwindow *const window, const int key, const int scancode,
                               const int action, const int mode) {
  if (key == GLFW_KEY_ESCAPE) {
    glfwSetWindowShouldClose(window, GL_TRUE);
  }
  auto *app = static_cast<Application *>(glfwGetWindowUserPointer(window));
  app->game_.key_callback(window, key, scancode, action, mode);
}

void GlfwDestroyWindow::operator()(GLFWwindow *const ptr) const {
  if (ptr == nullptr) {
    return;
  }

  spdlog::get("Global")->debug("glfwDestroyWindow");
  glfwDestroyWindow(ptr);
}

// Value of ptr doesn't matter
void GlfwTerminate::operator()(const bool *const ptr) const {
  if (ptr == nullptr) {
    return;
  }

  spdlog::get("Global")->debug("glfwTerminate");
  glfwTerminate();
  // Smart pointer custom deleter
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  delete ptr;
}
