#include "ResourceManager.hpp"
#include "Util.hpp"
#include <experimental/filesystem>
#include <glad/glad.h>
#include <spdlog/spdlog.h>
#include <stb_image.h>

namespace fs = std::experimental::filesystem;

std::shared_ptr<Shader> ResourceManager::load_shader(std::string name,
                                                     const std::string &vertex_path,
                                                     const std::string &fragment_path) {
  const std::string full_vertex_path =
      (fs::path(root_asset_shader_path) / fs::path(vertex_path)).string();
  const std::string full_fragment_path =
      (fs::path(root_asset_shader_path) / fs::path(fragment_path)).string();

  const std::string vertex_source = Util::read_file_as_string(full_vertex_path);
  const std::string fragment_source = Util::read_file_as_string(full_fragment_path);

  auto shader = std::make_shared<Shader>(vertex_source, fragment_source);
  shaders_.emplace(std::move(name), shader);
  return shader;
}

std::shared_ptr<Shader> ResourceManager::get_shader(const std::string &name) const {
  return shaders_.at(name);
}

std::shared_ptr<Texture> ResourceManager::load_texture(std::string name, const std::string &path) {
  const std::string full_path = (fs::path(root_asset_texture_path) / fs::path(path)).string();

  int width, height, channels;
  GLubyte *data = stbi_load(full_path.c_str(), &width, &height, &channels, 0);
  if (data == nullptr) {
    throw std::runtime_error("Failed to load texture: " + full_path);
  }
  const GLenum format = [&]() -> GLenum {
    if (channels == 3) {
      return GL_RGB;
    }
    if (channels == 4) {
      return GL_RGBA;
    }
    throw std::runtime_error("Unknown image format for: " + full_path);
  }();

  auto texture = std::make_shared<Texture>(width, height, format, data, TextureType::Diffuse);
  textures_.emplace(std::move(name), texture);
  stbi_image_free(data);
  return texture;
}

std::shared_ptr<Texture> ResourceManager::get_texture(const std::string &name) const {
  return textures_.at(name);
}

spdlog::logger *ResourceManager::logger() {
  static auto instance = spdlog::stdout_color_mt("ResourceManager");
  return instance.get();
}
