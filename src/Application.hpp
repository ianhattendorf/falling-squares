#pragma once

#include "Game.hpp"
#include "spdlog_fwd.hpp"

struct GlfwDestroyWindow {
  void operator()(GLFWwindow *ptr) const;
};

struct GlfwTerminate {
  void operator()(const bool *ptr) const;
};

class Application {
public:
  explicit Application(int msaa_samples);
  Application(const Application &) = delete;
  Application(Application &&) = delete;
  Application &operator=(const Application &) = delete;
  Application &operator=(Application &&) = delete;
  ~Application();
  void run();

private:
  static spdlog::logger *logger();
  static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mode);

  // GLFWwindow needs to be destroyed before glfwTerminate is called.
  // Encapsulate glfwInit status in unique_ptr with custom deleter.
  std::unique_ptr<bool, GlfwTerminate> glfw_context_;
  std::unique_ptr<GLFWwindow, GlfwDestroyWindow> window_;
  Game game_;

  const int msaa_samples_;

  static constexpr int INITIAL_WIDTH = 640;
  static constexpr int INITIAL_HEIGHT = 480;
};
