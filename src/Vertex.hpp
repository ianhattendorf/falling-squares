#pragma once

#include <glm/vec2.hpp>

struct Vertex {
  glm::vec2 position;
  glm::vec2 texture_coordinates;

  Vertex(float px, float py, float tx, float ty) : position(px, py), texture_coordinates(tx, ty) {}
};
