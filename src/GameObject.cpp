#include "GameObject.hpp"
#include "SpriteRenderer.hpp"
#include "opengl/Texture.hpp"
#include <glm/gtc/matrix_transform.hpp>

GameObject::GameObject(const glm::vec2 &position, const glm::vec2 &size, const glm::vec2 &velocity,
                       const glm::vec3 &color, std::shared_ptr<Texture> texture)
    : position_(position), size_(size), velocity_(velocity), color_(color), rotation_(0.0f),
      visible_(true), texture_(std::move(texture)) {}

void GameObject::render(const SpriteRenderer *const renderer) const {
  assert(texture_ != nullptr);
  if (!visible_) {
    return;
  }
  renderer->draw_sprite(texture_.get(), get_model(), color_);
}

glm::mat4 GameObject::get_model() const {
  glm::mat4 model{1.0f};

  model = glm::translate(model, glm::vec3{position_, 0.0f});

  model = glm::translate(model, glm::vec3{0.5f * size_.x, 0.5f * size_.y, 0.0f});
  model = glm::rotate(model, rotation_, glm::vec3{0.0f, 0.0f, 1.0f});
  model = glm::translate(model, glm::vec3{-0.5f * size_.x, -0.5f * size_.y, 0.05});

  model = glm::scale(model, glm::vec3{size_, 1.0f});

  return model;
}

bool GameObject::collides(const GameObject &other) const {
  // first.right_edge >= second.left_edge && second.right_edge >= first.left_edge
  const bool x_collision = position_.x + size_.x >= other.position_.x &&
                           other.position_.x + other.size_.x >= position_.x;
  // first.bottom_edge >= second.top_edge && second.bottom_edge >= first.top_edge
  const bool y_collision = position_.y + size_.y >= other.position_.y &&
                           other.position_.y + other.size_.y >= position_.y;

  return x_collision && y_collision;
}
