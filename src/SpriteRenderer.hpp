#pragma once

#include "opengl/Buffer.hpp"
#include "opengl/VertexArray.hpp"
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <memory>

class Shader;
class Texture;

class SpriteRenderer {
public:
  explicit SpriteRenderer(std::shared_ptr<Shader> shader);

  void draw_sprite(const Texture *texture, const glm::mat4 &model, const glm::vec3 &color) const;

private:
  std::shared_ptr<Shader> shader_;
  VertexArray vao_;
  Buffer vbo_;
};
