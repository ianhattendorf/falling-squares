#include "GameLevel.hpp"
#include "ResourceManager.hpp"
#include <random>

GameLevel::GameLevel(const int num_squares, const int width, const int height,
                     const ResourceManager *const resource_manager) {
  constexpr int square_width = 50;
  constexpr int square_height = 50;
  const auto texture = resource_manager->get_texture("square");

  std::random_device rd;
  std::seed_seq seed_seq{rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd()};
  std::mt19937 random_engine{seed_seq};
  std::uniform_int_distribution<int> ypos_dist{-(height - square_height) * 10, 0};
  std::uniform_int_distribution<int> xpos_dist{0, width - square_width};
  std::uniform_real_distribution<float> speed_dist{150.0f, 200.0f};
  std::uniform_real_distribution<float> color_dist{0.1f, 1.0f};

  for (int i = 0; i < num_squares; ++i) {
    const glm::vec2 position{xpos_dist(random_engine), ypos_dist(random_engine)};
    const glm::vec2 size{square_width, square_height};
    const glm::vec2 velocity{0.0f, speed_dist(random_engine)};
    const glm::vec3 color{color_dist(random_engine), color_dist(random_engine),
                          color_dist(random_engine)};
    squares_.emplace_back(position, size, velocity, color, texture);
  }
}

void GameLevel::render(const SpriteRenderer *const renderer) const {
  for (const auto &square : squares_) {
    square.render(renderer);
  }
}
