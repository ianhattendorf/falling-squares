# falling-squares

Simple falling squares game. Squares fall. Try to dodge them.

## Building

Requires a compiler that supports C++17. Tested on GCC 7.3, Clang 5/6, and VS 2017.

### Install Dependencies

- Microsoft Guidelines Support Library
- GLFW 3
- GLM
- Ogg
- Vorbis
- Sndfile
- spdlog
  - fmt

#### Linux

##### Fedora

- spdlog-devel and fmt-devel are probably out of date, so you might need to install manually and set spdlog_DIR and fmt_DIR CMAKE variables.
- Install the [Microsoft Guidelines Support Library](https://github.com/Microsoft/GSL)

```sh
# dnf install glfw-devel glm-devel openal-soft-devel libogg-devel libvorbis-devel libsndfile-devel
```

##### Debian/Ubuntu

- libspdlog-dev and libfmt-dev are probably out of date, so you might need to install manually and set spdlog_DIR and fmt_DIR CMAKE variables.

```sh
# apt install libglfw3-dev libglm-dev libopenal-dev libogg-dev libvorbis-dev libsndfile1-dev
```

#### Windows

- Install [vcpkg](https://github.com/Microsoft/vcpkg)

```powershell
PS:\> vcpkg install --triplet x64-windows ms-gsl glfw3 glm openal-soft libogg libvorbis libsndfile spdlog fmt
```

### Generate Build Files

#### Linux

- If using manually installed spdlog/fmt, set -Dspdlog_DIR and -Dfmt_DIR to point to them.
- Set -Dmsgsl_INCLUDE_DIRS

```sh
$ cd <project_root>
$ mkdir build && cd build && cmake -Dmsgsl_INCLUDE_DIRS="/path/to/msgsl/include" -DCMAKE_BUILD_TYPE=Debug ..
```

#### Windows

- Need to help CMake find ms-gsl and libsndfile

```powershell
PS:\> cd <project_root>
PS:\> mkdir build; cd build
PS:\> cmake -DCMAKE_BUILD_TYPE=Debug -A=x64 -DCMAKE_TOOLCHAIN_FILE="<vcpkg_root>\scripts\buildsystems\vcpkg.cmake" Dmsgsl_INCLUDE_DIRS="<vcpkg_root>\installed\x64-windows\include" -Dsndfile_INCLUDE_DIRS="<vcpkg_root>\installed\x64-windows\include" -Dsndfile_LIBRARY="<vcpkg_root>\installed\x64-windows\lib\libsndfile-1.lib" ..
```

### Compile

#### Linux

```sh
$ cd <project_root>/build
$ make -j$(nproc)
```

#### Windows

NOTE: Requires MSBuild to be in the path, usually accomplished by opening the `Developer Command Prompt for VS <version>`.

```powershell
PS:\> cd <project_root>\build
PS:\> msbuild falling_squares.sln
```
