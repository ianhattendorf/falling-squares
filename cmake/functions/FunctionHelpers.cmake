function(message_var var_name_to_print)
    message(STATUS "${var_name_to_print}: " ${${var_name_to_print}})
endfunction()
