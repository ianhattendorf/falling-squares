# Find sndfile
#
# Defined targets:
#  sndfile::sndfile

find_path(sndfile_INCLUDE_DIRS
        DOC "Path to sndfile include directory."
        NAMES sndfile.hh)

find_library(sndfile_LIBRARY
        DOC "Path to sndfile library."
        NAMES sndfile)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(sndfile REQUIRED_VARS sndfile_INCLUDE_DIRS sndfile_LIBRARY)

add_library(sndfile::sndfile UNKNOWN IMPORTED)
set_target_properties(sndfile::sndfile PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${sndfile_INCLUDE_DIRS}
        IMPORTED_LOCATION ${sndfile_LIBRARY})
