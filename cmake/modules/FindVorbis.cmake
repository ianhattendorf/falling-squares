# Find Vorbis
#
# TODO component aware?
#
# Defined targets:
#  Vorbis::Vorbis

find_path(Vorbis_INCLUDE_DIRS
        DOC "Path to Vorbis include directory."
        NAMES vorbis/vorbisfile.h)

find_library(Vorbis_LIBRARY
        DOC "Path to vorbis library."
        NAMES vorbis)
find_library(VorbisEnc_LIBRARY
        DOC "Path to vorbisenc library."
        NAMES vorbisenc)
find_library(VorbisFile_LIBRARY
        DOC "Path to vorbisfile library."
        NAMES vorbisfile)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Vorbis REQUIRED_VARS Vorbis_INCLUDE_DIRS Vorbis_LIBRARY VorbisEnc_LIBRARY VorbisFile_LIBRARY)

if(NOT TARGET Vorbis::Vorbis)
    add_library(Vorbis::Vorbis UNKNOWN IMPORTED)
    set_target_properties(Vorbis::Vorbis PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES ${Vorbis_INCLUDE_DIRS}
            IMPORTED_LOCATION ${Vorbis_LIBRARY})
#            IMPORTED_LINK_INTERFACE_LIBRARIES "${VorbisEnc_LIBRARY} ${VorbisFile_LIBRARY}")
endif()

if(NOT TARGET Vorbis::VorbisEnc)
    add_library(Vorbis::VorbisEnc UNKNOWN IMPORTED)
    set_target_properties(Vorbis::VorbisEnc PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES ${Vorbis_INCLUDE_DIRS}
            IMPORTED_LOCATION ${VorbisEnc_LIBRARY})
endif()

if(NOT TARGET Vorbis::VorbisFile)
    add_library(Vorbis::VorbisFile UNKNOWN IMPORTED)
    set_target_properties(Vorbis::VorbisFile PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES ${Vorbis_INCLUDE_DIRS}
            IMPORTED_LOCATION ${VorbisFile_LIBRARY})
endif()

add_dependencies(Vorbis::VorbisEnc Vorbis::Vorbis)
add_dependencies(Vorbis::VorbisFile Vorbis::Vorbis)
