# Find msgsl
#
# Defined targets:
#  msgsl::msgsl

find_path(msgsl_INCLUDE_DIRS
        DOC "Path to msgsl include directory."
        NAMES gsl/gsl)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(msgsl REQUIRED_VARS msgsl_INCLUDE_DIRS)

add_library(msgsl::msgsl INTERFACE IMPORTED)
set_target_properties(msgsl::msgsl PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${msgsl_INCLUDE_DIRS})
