# Find Ogg
#
# Defined targets:
#  Ogg::Ogg

find_path(Ogg_INCLUDE_DIRS
        DOC "Path to Ogg include directory."
        NAMES ogg/ogg.h)

find_library(Ogg_LIBRARY
        DOC "Path to Ogg library."
        NAMES ogg)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Ogg REQUIRED_VARS Ogg_INCLUDE_DIRS Ogg_LIBRARY)

add_library(Ogg::Ogg UNKNOWN IMPORTED)
set_target_properties(Ogg::Ogg PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${Ogg_INCLUDE_DIRS}
        IMPORTED_LOCATION ${Ogg_LIBRARY})
